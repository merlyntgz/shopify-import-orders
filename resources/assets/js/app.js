
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


require('./directives');

const routes = [
  { path: '/', component: require('./components/FileList.vue')}
];

Vue.component('h-loader', require('./components/HLoader.vue'));
Vue.component('errors', require('./components/Errors.vue'));
var router = new VueRouter({
    routes
});

const app = new Vue({
    el: '#app',
    created: function() {
        $(document).foundation();
        ShopifyApp.init({
            apiKey: Laravel.shopify.key,
            shopOrigin: 'https://' + Laravel.shopify.domain
        });
        ShopifyApp.ready(function () {
            ShopifyApp.Bar.loadingOff();
            ShopifyApp.Bar.setTitle('Orders import tool');
        });
    },
    router
});
