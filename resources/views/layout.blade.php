<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Orders import</title>
  <link type="text/css" rel="stylesheet" href="/css/app.css">
  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
    ShopifyApp.init({
      apiKey: '6e88da32c62befb6b73f376e3df3cfa0',
      shopOrigin: 'https://yaroslavdemoshop.myshopify.com'
    });
    ShopifyApp.ready(function () {
      ShopifyApp.Bar.loadingOff();
    });
  </script>
</head>
<body>
  <div class="row">
    <div class="small-3 columns">
      <ul class="menu vertical">
        <li><a href="/">Add file</a></li>
        <li><a href="/list">Files list</a></li>
      </ul>
    </div>
    <div class="small-9 columns">
      <h3>@yield('title')</h3>
      <div>
        @yield('content')
      </div>
    </div>
  </div>
</body>
</html>
