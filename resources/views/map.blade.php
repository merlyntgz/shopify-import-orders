@extends('layout')

@section('title')
  Map file properties
@endsection

@section('map-options')
  <option selected="selected" value="">-----</option>
  @foreach($headings as $heading)
    <option value="{{ $heading }}">
      {{ $heading }}
    </option>
  @endforeach
@endsection

@section('content')
<form method="post" action="/import/{{ $id }}">
  {{ csrf_field() }}

  <div class="row">
    <div class="small-12 columns text-center">
      <h5>Order properties</h5>
    </div>
  </div>

  <div class="row">
    @each('field', $sections['self'], 'row')
  </div>

  <div class="row">
    <div class="small-12 columns text-center">
      <h5>Customer properties</h5>
    </div>
  </div>

  <div class="row">
    @each('field', $sections['customer'], 'row')
  </div>

  <div class="row">
    <div class="small-12 columns text-center">
      <h5>Billing address properties</h5>
    </div>
  </div>

  <div class="row">
    @each('field', $sections['billing_address'], 'row')
  </div>

  <div class="row">
    <div class="small-12 columns text-center">
      <h5>Shipping address properties</h5>
    </div>
  </div>

  <div class="row">
    @each('field', $sections['shipping_address'], 'row')
  </div>

  <div class="row">
    <div class="small-12 columns text-center">
      <h5>Line items properties</h5>
    </div>
  </div>

  <div class="row">
    @each('field', $sections['line_items'], 'row')
  </div>

  <div class="row">
    <div class="small-12 columns text-center">
      <button type="submit" class="success button">Import</button>
    </div>
  </div>

</form>
@endsection
