@extends('layout')

@section('title')
  Import file
@endsection

@section('content')
  <form method="post" action="/upload" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
      <div class="columns small-12">
        <label for="fileInput" class="button">Select file for import</label>
        <input id="fileInput" type="file" name="sheet" class="show-for-sr">
      </div>
    </div>
    <div class="row">
      <div class="columns small-12">
        <button type="submit" class="success button">Save</button>
      </div>
    </div>
  </form>
@endsection
