<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Orders import</title>
  <script src="https://use.fontawesome.com/cc90fff5df.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="{{ elixir('css/app.css') }}">
</head>
<body>
    <div class="row">
        <div id="app" class="small-12 columns">
            <router-view></router-view>
        </div>
    </div>
    <script>
        window.Laravel = {
            csrfToken: '{{ csrf_token() }}',
            shopify: {
                key: '{{ config("shopify.key") }}',
                domain: '{{ $currentToken->domain }}',
            }
        };
    </script>
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
