@extends('layout')

@section('title')
  Files list
@endsection

@section('content')
  @foreach($files as $file)
    <div>
      {{ $file->name }}
      <a href="/process/{{ $file->id }}">Process</a>
      <a href="/delete/{{ $file->id }}">Delete</a>
    </div>
  @endforeach
@endsection
