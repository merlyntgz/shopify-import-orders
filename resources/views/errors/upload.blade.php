@extends('layout')

@section('title')
  Import file failed
@endsection

@section('content')
  <div class="box notice">
    <i class="ico-warning"></i>{{ $message }}
  </div>
@endsection
