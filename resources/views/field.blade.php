<div class="columns small-6">
  <div class="row">
    <div class="columns small-4">
      <label for="order_{{ $row['alias'] }}" class="text-right middle">{{ $row['title'] }}</label>
    </div>
    <div class="columns small-8">
      <select id="order_{{ $row['alias'] }}" name="order{{ $row['name'] }}">
        @yield('map-options')
      </select>
    </div>
  </div>
</div>
