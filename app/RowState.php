<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RowState extends Model
{
    protected $fillable = ['file_id', 'row_id', 'order_id'];

    public function file()
    {
        return $this->belongsTo('App\File');
    }

    public function scopeFailed($query) {
        return $query->whereNull('order_id');
    }
}
