<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileOrder extends Model
{
    protected $fillable = ['file_id', 'order_id'];

    public function file()
    {
        return $this->belongsTo('App\File');
    }
}
