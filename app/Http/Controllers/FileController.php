<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(File::orderBy('id', 'desc')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        if (!$r->hasFile('sheet')) {
            return response()->json(['message' => 'File is missing'], 400);
        }
        $file = $r->sheet;
        if (!in_array($file->getClientOriginalExtension(), ['xls', 'xlsx', 'csv'])) {
            return response()->json(['message' => 'Unsupported file type. Supported types: xls, xlsx, csv'], 400);
        }
        $dbFile = new File();
        $dbFile->data = file_get_contents($file->getRealPath());
        $dbFile->name = $file->getClientOriginalName();
        $dbFile->save();
        $xls = $dbFile->xls();
        $dbFile->count = $xls->get()->count();
        $dbFile->save();
        return response()->json($dbFile);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        return response()->json($file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        $file->delete();
        return response()->json(['message' => 'File deleted']);
    }
}
