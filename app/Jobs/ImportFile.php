<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;



class ImportFile implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $_file;
    protected $_map;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file, $map)
    {
        $this->_file = $file;
        $this->_map = $map;
        $file->setImportState('queued');
        $file->countImport(0, 0);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $f = $this->_file;
        $f->countImport(0, 0);
        $f->setImportState('run');
        $map = $this->_map;
        $xls = $f->xls();
        $failed = [];
        $imported = [];
        $xls->each(function($row) use ($f, $map, &$imported, &$failed) {
            try {
                $order = $f->createOrderByMap($row, $map);
                $imported[] = [
                    'row' => $row,
                    'order' => $order
                ];
            } catch (ShopifyException $se) {
                $failed[] = [
                    'row' => $row,
                    'error' => $se->getResponse()
                ];
            }
            $f->countImport(count($imported), count($failed));
        });
    }
}
