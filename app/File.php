<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yarsky\Shopify\Scopes\TokenScope;
use Yarsky\Shopify\Order;
use Excel;
use Redis;

class File extends Model
{
    const TEMP_PATH = '/tmp/';

    protected $casts = [
        'is_imported' => 'boolean',
    ];

    public static function getKeySections()
    {
        return [
          'self' => [
            'id' => ['title' => 'ID'],
            'subtotal_price' => ['title' => 'Subtotal price'],
            'note' => ['title' => 'Note'],
            'note_attributes' => ['title' => 'Note attributes'],
            'currency' => ['title' => 'Currency'],
            'total_discounts' => ['title' => 'Total discounts'],
            'total_tax' => ['title' => 'Total tax'],
            'financial_status' => ['title' => 'Financial status'],
            'browser_ip' => ['title' => 'Browser IP'],
            'payment_gateway_names' => ['title' => 'Payment gateway names'],
            'total_price' => ['title' => 'Total price'],
            'tags' => ['title' => 'Tags'],
            'processed_at' => ['title' => 'Processed at'],
            'total_weight' => ['title' => 'Total weight']
          ],
          'customer' => [
            'email' => ['title' => 'Email'],
            'password' => ['title' => 'Password']
          ],
          'billing_address' => [
            'country' => ['title' => 'Country'],
            'province' => ['title' => 'Province'],
            'zip' => ['title' => 'ZIP'],
            'city' => ['title' => 'City'],
            'address1' => ['title' => 'Address1'],
            'address2' => ['title' => 'Address2'],
            'first_name' => ['title' => 'First name'],
            'last_name' => ['title' => 'Last name'],
            'name' => ['title' => 'Name'],
            'phone' => ['title' => 'Phone'],
            'company' => ['title' => 'Company'],
            'country_code' => ['title' => 'Country code']
          ],
          'shipping_address' => [
            'country' => ['title' => 'Country'],
            'province' => ['title' => 'Province'],
            'zip' => ['title' => 'ZIP'],
            'city' => ['title' => 'City'],
            'address1' => ['title' => 'Address1'],
            'address2' => ['title' => 'Address2'],
            'first_name' => ['title' => 'First name'],
            'last_name' => ['title' => 'Last name'],
            'name' => ['title' => 'Name'],
            'phone' => ['title' => 'Phone'],
            'company' => ['title' => 'Company'],
            'country_code' => ['title' => 'Country code']
          ],
          'line_items' => [
            'sku' => ['title' => 'SKU'],
            'quantity' => ['title' => 'Quantity'],
            'grams' => ['title' => 'Grams'],
            'total_discount' => ['title' => 'Total discount'],
            'price' => ['title' => 'Price'],
            'fulfillment_status' => ['title' => 'Fulfillment status'],
            'fulfillment_service' => ['title' => 'Fulfillment service'],
            'taxable' => ['title' => 'Taxable']
          ]
        ];
    }

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new TokenScope);
    }

    public function rows()
    {
        return $this->hasMany('App\RowState');
    }

    public function createOrderByMap($row, $map)
    {
        $order = new Order();
        $sections = [];
        $line_item = [];
        foreach ($map as $key => $alias) {
            $value = $row[$alias];
            if (empty($value)) {
                continue;
            }
            $section = '';
            if (strpos($key, '.') !== false) {
                list($section, $name) = explode('.', $key);
                if ($section == 'line_items') {
                    $line_item[$name] = $value;
                } else {
                    if (!isset($sections[$section])) {
                        $sections[$section] = [];
                    }
                    $sections[$section][$name] = $value;
                }
            } else {
                $order->$key = $value;
            }
        }
        foreach ($sections as $alias => $data) {
            $order->$alias = $data;
        }
        $order->line_items = [$line_item];
        $order->fresh()->save();
        $this->orders()->create([
          'file_id' => $f->id,
          'order_id' => $order->id
        ]);
        return $order;
    }

    public function preload()
    {
        $tempName = self::TEMP_PATH . $this->id . '_' . $this->name;
        file_put_contents($tempName, $this->data);
        return $tempName;
    }

    public function xls()
    {
        $filename = $this->preload();
        return Excel::load($filename);
    }

    public function setImportState($state)
    {
        Redis::set('file:' . $this->id . ':state', $state);
        return $this;
    }

    public function getImportState()
    {
        return Redis::get('file:' . $this->id . ':state');
    }

    public function cleanImportState()
    {
        Redis::set('file:' . $this->id . ':state', null);
        return $this;
    }

    public function countImport($imported, $failed)
    {
        Redis::set('file:' . $this->id . ':count:imported', $imported);
        Redis::set('file:' . $this->id . ':count:failed', $failed);
        return $this;
    }

    public function getImportCounters()
    {
        return [
            'imported' => Redis::get('file:' . $this->id . ':count:imported'),
            'failed' => Redis::get('file:' . $this->id . ':count:failed')
        ];
    }
}
