<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\File;
use Yarsky\Shopify\Model\Token;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        File::creating(function ($file) {
            $file->token_id = Token::current()->id;
            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
