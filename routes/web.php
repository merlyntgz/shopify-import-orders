<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('vue', ['currentToken' => \Yarsky\Shopify\Model\Token::current()]);
});
// Route::post('/save', 'FileController@save')->name('save');
// Route::get('/map/{f}', 'FileController@map')->name('map');
// Route::get('/list', 'FileController@list')->name('list');
// Route::get('/watch/{f}', 'FileController@watch')->name('watch');
// Route::get('/import/state/{f}', 'FileController@state');
// Route::get('/delete/{f}', 'FileController@delete');
// Route::post('/queue/{f}', 'FileController@queue')->name('queue');
