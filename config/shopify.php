<?php

return [
    'key' => '6e88da32c62befb6b73f376e3df3cfa0',
    'secret' => '43b2e375863a77a5418070615bff4063',
    'permissions' => [
        'read_products',
        'write_products',
        'write_orders',
        'read_orders',
        'read_customers',
        'write_customers'
    ],
    'redirect' => 'https://evening-reaches-58165.herokuapp.com/install',
    'endpoint' => '/'
];
